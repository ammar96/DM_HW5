import pandas as pd
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA

x_train = pd.read_csv('X_train.csv')
x_train = x_train.drop(['total', 'customerAttr_a', 'state', 'customerAttr_b'], axis=1)
y_train = pd.read_csv('Y_train.csv')
x_test = pd.read_csv('X_test.csv')
x_test = x_test.drop(['total', 'customerAttr_a', 'state', 'customerAttr_b'], axis=1)

# delete redundant features
# corrMatrix = x_train.corr()
# corrMatrix = corrMatrix.as_matrix(columns=None)
# list = []
# for i in range(len(corrMatrix[0])):
#     for j in range(i + 1, len(corrMatrix[0])):
#         if corrMatrix[i][j] >= .9:
#             if i not in list:
#                 list.append(i)
#             else:
#                 list.append(j)
# for i in len(list):
#     x_train.drop()

# D.T.
# clf = tree.DecisionTreeClassifier()
# clf = clf.fit(x_train, y_train)
# clf.predict(x_test)

# random forest
# rf = RandomForestClassifier(n_estimators=100)  # initialize
# rf.fit(x_train, y_train.values.ravel())  # fit the data to the algorithm
# rf.predict(x_test)

# N.N.
# clf = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
# clf.fit(x_train, y_train.values.ravel())
# clf.predict(x_test)

# L.R.
# lr = LogisticRegression()
# lr = lr.fit(x_train, y_train.values.ravel())
# lr.predict(x_test)

# 5 PCA
pca = PCA(n_components=2)
pca.fit(x_train)
pca.transform(x_train)
pca.transform(x_test)
